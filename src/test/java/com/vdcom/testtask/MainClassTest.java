package com.vdcom.testtask;

import com.vdcom.testtask.exceptions.IncorrectInputNumberException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainClassTest {

    @Test
    public void applicationTest() {
        String[] firstArgs = {"100"};
        String[] secondArgs = {"1212"};
        long actual;
        try {
            MainClass.main(firstArgs);
            String fileContent = readFileContent();
            if (fileContent == null) Assertions.fail("File is empty.");
            actual = Long.parseLong(fileContent);
            Assertions.assertEquals(100, actual);
            MainClass.main(secondArgs);
            fileContent = readFileContent();
            if (fileContent == null) Assertions.fail("File is empty.");
            actual = Long.parseLong(fileContent);
            Assertions.assertEquals(1212, actual);
        } catch (IOException | NumberFormatException ex) {
            ex.printStackTrace();
            Assertions.fail("Error during reading file content.");
        }
    }

    @Test
    public void incorrectInputTest() {
        String[] args = {"some text"};
        Assertions.assertThrows(RuntimeException.class, () -> {
            MainClass.main(args);
        });
        String[] secondArgs = {"55"};
        Assertions.assertThrows(IncorrectInputNumberException.class, () -> {
           MainClass.main(secondArgs);
        });
        String[] thirdArgs = {"-10"};
        Assertions.assertThrows(IncorrectInputNumberException.class, () -> {
            MainClass.main(thirdArgs);
        });
    }

    private String readFileContent() throws IOException {
        File file = new File(MainClass.FILE_NAME);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            return reader.readLine();
        }
    }
}
