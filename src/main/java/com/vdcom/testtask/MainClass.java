package com.vdcom.testtask;

import com.vdcom.testtask.exceptions.IncorrectInputNumberException;
import java.io.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class MainClass {

    public static final String FILE_NAME = "out.txt";

    public static void main(String[] args) {
        long inputNumber = checkAndGetInputParameter(args);
        File file = new File(FILE_NAME);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("0");
        } catch (IOException ex) {
            System.out.println("Error while writing to file.");
            ex.printStackTrace();
            return;
        }
        Runnable incrementRunnable = () -> {
            while (true) {
                synchronized (file) {
                    long currentNumber;
                    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                        currentNumber = Long.parseLong(reader.readLine());
                        if (currentNumber == inputNumber) break;
                    } catch (IOException | NumberFormatException ex) {
                        System.out.println("Error while reading number from file.");
                        ex.printStackTrace();
                        break;
                    }
                    ++currentNumber;
                    System.out.println(Thread.currentThread().getName()
                            + String.format(" : oldValue = %d, newValue = %d", (currentNumber - 1), currentNumber));
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                        writer.write(Long.toString(currentNumber));
                    } catch (IOException ex) {
                        System.out.println("Error while writing to file.");
                        ex.printStackTrace();
                        break;
                    }
                }
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        IntStream.rangeClosed(1, 2).forEach(i -> service.execute(incrementRunnable));
        service.shutdown();
        try {
            service.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            System.out.println("Error while threads execution.");
            ex.printStackTrace();
            return;
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String localString = reader.readLine();
            if (localString == null)
                System.out.println("File is empty.");
            else
                System.out.println("File content:" + localString);
        } catch (IOException ex) {
            System.out.println("Error while reading from file.");
            ex.printStackTrace();
        }
    }

    private static long checkAndGetInputParameter(String[] args) {
        if (args.length != 1)
            throw new RuntimeException("Incorrect number of input parameters.");
        long inputNumber;
        try {
            inputNumber = Long.parseLong(args[0]);
            if (inputNumber <= 0 || inputNumber % 2 == 1)
                throw new IncorrectInputNumberException("Incorrect input number");
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Incorrect input number. Number must be greater than 0 and even.");
        }
        return inputNumber;
    }
}
