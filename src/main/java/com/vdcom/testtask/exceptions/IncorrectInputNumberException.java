package com.vdcom.testtask.exceptions;

public class IncorrectInputNumberException extends RuntimeException {
    public IncorrectInputNumberException(String message) {
        super(message);
    }
}
